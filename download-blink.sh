#!/bin/bash

#Folder in ~ for Blink auth/cred files to be saved
CONFIGDIR="$HOME/.blink"
CONFIGFILE="blink.conf"
#vvvvvvvvvvvv blink.conf contents vvvvvvvvvvvv
#    EMAIL=email@address.com
#    PASSEWORD=^my#1*blink*password$
#    OUTPUTDIR=/full_path/to_save/directory
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#Path to jquery. Need JQuery to parse output
jq=/usr/local/bin/jq


#API endpoint
URL="rest.prod.immedia-semi.com"
URL_SUBDOMAIN="prod"
TIMEZONE=":US/Eastern"


#read values from configuration file
OUTPUTDIR=$(grep OUTPUTDIR ${CONFIGDIR}/${CONFIGFILE} | cut -d '=' -f 2)
EMAIL=$(grep EMAIL ${CONFIGDIR}/${CONFIGFILE} | cut -d '=' -f 2)
PASSWORD=$(grep PASSWORD ${CONFIGDIR}/${CONFIGFILE} | cut -d '=' -f 2)


preReq () {
    if ! [ -x "$(command -v $jq)" ]; then
        clear
        echo
        echo "Error: jq package not detected..."
        echo
        echo "     Please install the jq package for your system:"
        echo "           https://stedolan.github.io/jq/ " 
        echo
        exit
    else
        JQ=true      
    fi
}


credGet () {
    # if present Read the cached URL from config directory
    URL2=$(cat ${CONFIGDIR}/url 2>/dev/null)
    if [ ! "${URL2}" == "" ]; then 
        URL=${URL2}
    fi

    # Read the cached auth code
    AUTHCODE=$(cat ${CONFIGDIR}/authcode 2>/dev/null) 
    AUTHTEST=$(curl -s -H "Host: ${URL}" -H "TOKEN_AUTH: ${AUTHCODE}" --compressed https://${URL}/homescreen | grep -o '\"message\":\".\{0,12\}' | cut -c12-)
    if [ "${AUTHTEST}" == "Unauthorized" ]; then 
        # authenticate to blink with the credentials
        AUTH=$(curl -s -H "Host: ${URL}" -H "Content-Type: application/json" --data-binary '{ "password" : "'"${PASSWORD}"'", "client_specifier" : "iPhone 9.2 | 2.2 | 222", "email" : "'"${EMAIL}"'" }' --compressed https://${URL}/login )
        # get authcode from output
        AUTHCODE=$(echo $AUTH | grep -o '\"authtoken\":\".\{0,22\}' | cut -c14-)
        # store authcode in config directory
        echo $AUTHCODE > ${CONFIGDIR}/authcode
        if [ "${AUTHCODE}" == "" ]; then
            echo "No Authcode received, please check credentials"
            exit
        fi

        # Read the domain, adjust and cache the URL
        SUBDOMAIN=$(echo $AUTH | grep -o '\"region\":{"[^"]\+"' | cut -c12- | grep -o '[[:alnum:]]*')
        URL=${URL/.${URL_SUBDOMAIN}./.${SUBDOMAIN}.}
        echo $URL > ${CONFIGDIR}/url
    fi

    # Query the network ID
    NETWORKID=$(curl -s -H "Host: ${URL}" -H "TOKEN_AUTH: ${AUTHCODE}" --compressed https://${URL}/networks | grep -o '\"summary\":{\".\{0,6\}' | cut -c13- | grep -o '[[:digit:]]*')
    echo Network ID: ${NETWORKID}
}

download () {

    echo;echo "Download all videos"
    echo "AUTHCODE = ${AUTHCODE}"
    echo "URL = ${URL}"
    COUNT=$(curl -s -H "Host: ${URL}" -H "TOKEN_AUTH: ${AUTHCODE}" --compressed https://${URL}//api/v2/videos/count | sed -n 's/\"count"\://p' | tr -d '{}')
    echo "Total clips = ${COUNT}"
    COUNT=$(((${COUNT} / 10)+2))
    for ((n=0;n<${COUNT};n++)); do
        VIDEOS=$(curl -s -H "Host: ${URL}" -H "TOKEN_AUTH: ${AUTHCODE}" --compressed https://${URL}//api/v2/videos/page/${n} | $jq -c '.[] | { address: .address, id: .id }')
        for VIDEO in $VIDEOS; do
            ADDRESS=$(echo $VIDEO | $jq -r '.address')
            ID=$(echo $VIDEO | $jq -r '.id')
            ADDRESS2=$( echo $ADDRESS | sed 's:.*/::' )
            PAD=$(echo $ADDRESS | tr -dc '_' | awk '{ print length; }')
            ADDRESS3=$(echo $ADDRESS2 | cut -d '_' -f $(($PAD-4))-99)
            CAMERA=$(dirname $ADDRESS | xargs basename)
            DATESTAMP=$(echo $ADDRESS3 | grep -Eo '[0-9]{1,4}' | tr -d '\n' | sed 's/.$//')
            DATESTAMP2=$( TZ=${TIMEZONE} date -j -f %Y%m%d%H%M%z ${DATESTAMP}+0000 +%Y%m%d%H%M )
            ADDRESS3_FILENAME="${ADDRESS3%.*}"
            ADDRESS3_EXTENSION="${ADDRESS3##*.}"
            ADDRESS4=${ADDRESS3_FILENAME}-${CAMERA}
            ADDRESS5=${ADDRESS4}-${ID}.${ADDRESS3_EXTENSION}
            
            ls ${OUTPUTDIR}/${ADDRESS5} &> /dev/null
            if ! [ $? -eq 0 ]; then
                echo "Downloading ${ADDRESS5} to ${OUTPUTDIR} with timestamp ${DATESTAMP}"
                # download the file
                curl -s -H "Host: ${URL}" -H "TOKEN_AUTH: ${AUTHCODE}" --compressed https://${URL}/${ADDRESS} > ${OUTPUTDIR}/${ADDRESS5}
                # touch the file so it appears with the right datestamp
                TZ=UTC touch -a -m -t ${DATESTAMP2} ${OUTPUTDIR}/${ADDRESS5}
                # Print in green
                tput setaf 2
                echo "[ ** ${ADDRESS5} is new! ** ]"
                # Print back in black
                tput sgr0
            fi
        done 
    done
    echo "Download complete. Your videos can be found here: ${OUTPUTDIR}"
    exit
}


    preReq;
    credGet;
    download;

